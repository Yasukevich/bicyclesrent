package by.epam.bicyclesrent.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;


/**
 * Сlass HeaderTag.
 */
public class HeaderTag extends TagSupport {
    private String user;

    public void setRole(String role) {
        this.user = role;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            String hello = null;
            if ("root".equalsIgnoreCase(user)) {
                hello = "Hello, " + user;
            } else {
                hello = "Welcome " + user;
            }
            pageContext.getOut().write("<hr/>" + hello + "<hr/>");
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
