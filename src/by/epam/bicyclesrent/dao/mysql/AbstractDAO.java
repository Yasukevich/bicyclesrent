package by.epam.bicyclesrent.dao.mysql;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * class AbstractDAO
 */
public abstract class AbstractDAO {
    /**
     * Logger field.
     */
    protected static Logger logger;
    /**
     * Connection field.
     */
    protected Connection connection;
    protected PreparedStatement preparedStatement = null;
    protected ResultSet resultSet = null;

    /**
     * Class constructor.
     * 
     * @param connection to set connection
     */
    public AbstractDAO(Connection connection) {
        this.connection = connection;
        logger = LogManager.getLogger(AbstractDAO.class.getName());
    }

}
