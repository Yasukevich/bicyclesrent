package by.epam.bicyclesrent.dao.mysql;

import by.epam.bicyclesrent.dao.UserDAO;
import by.epam.bicyclesrent.entity.Role;
import by.epam.bicyclesrent.entity.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;

/**
 * @author user
 */
public class UserDAOImpl extends AbstractDAO implements UserDAO {
    /**
     * List of users.
     */
    private static List<User> users;
    /**
     * Item of user.
     */
    private static User user;

    private final String CREATE_QUERY = "INSERT INTO `user` (`login`, `password`, `role`, `passport`, `money`, `is_blocked`) " +
            " VALUES (?, ?, 'renter', ?, ?, 1);";
    private final String ALL_QUERY = "SELECT * FROM user;";
    private final String DEL_QUERY = "DELETE FROM user WHERE id=?;";
    private final String FIND_BY_NAME_QUERY = "SELECT * FROM user WHERE login=? and password=?;";
    private final String FIND_BY_ID_QUERY = "SELECT * FROM user WHERE id=?;";
    private final String UPDATE_QUERY = "UPDATE user SET login=?, password=?, passport=?, role=?, money=?, is_blocked=? where id=?;";

    /**
     * @param connection
     */
    public UserDAOImpl(Connection connection) {
        super(connection);
    }

    /**
     * @see by.epam.bicyclesrent.dao.AbstractDAO#findAll()
     */
    @Override
    public List<User> findAll() {
        users = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(ALL_QUERY);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user = new User();
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setPassport(resultSet.getString("passport"));
                switch (resultSet.getString("role")) {
                    case "ADMIN":
                        user.setRole(Role.ADMIN);
                        break;
                    case "RENTER":
                        user.setRole(Role.RENTER);
                        break;
                    default:
                        break;
                }
                user.setMoney(Float.valueOf(resultSet.getString("money")));
                user.setBlocked(Boolean.parseBoolean(resultSet.getString("is_blocked")));
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            logger.log(Level.ERROR, e.getMessage());
            return users;
        } finally {
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
    }

    @Override
    public boolean findUserByNameAndPass(String name, String enterPass) {
        try {
            preparedStatement = connection.prepareStatement(FIND_BY_NAME_QUERY);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, enterPass);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            logger.log(Level.ERROR, e.getMessage());
            return false;
        } finally {
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
    }

    /**
     * @see by.epam.bicyclesrent.dao.DAO#findEntityById(java.lang.Object)
     */
    @Override
    public User findEntityById(Integer id) {
        user = new User();
        try {
            preparedStatement = connection.prepareStatement(FIND_BY_ID_QUERY);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                user.setId(id);
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setPassport(resultSet.getString("passport"));
                switch (resultSet.getString("role")) {
                    case "ADMIN":
                        user.setRole(Role.ADMIN);
                        break;
                    case "RENTER":
                        user.setRole(Role.RENTER);
                        break;
                    default:
                        break;
                }
                user.setMoney(Float.valueOf(resultSet.getString("money")));
                user.setBlocked(Boolean.parseBoolean(resultSet.getString("is_blocked")));
            }

            return user;
        } catch (SQLException e) {
            logger.log(Level.ERROR, e.getMessage());
            return user;
        } finally {
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
    }

    /**
     * @see by.epam.bicyclesrent.dao.DAO#delete(java.lang.Object)
     */
    @Override
    public boolean delete(Integer id) {
        try {
            preparedStatement = connection.prepareStatement(DEL_QUERY);
            preparedStatement.setInt(1, id);

            int count = preparedStatement.executeUpdate();

            if (count > 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            logger.log(Level.ERROR, e.getMessage());
            return false;
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
    }

    /**
     * @see by.epam.bicyclesrent.dao.DAO#create(by.epam.bicyclesrent.entity.AppEntity)
     */
    @Override
    public boolean create(User entity) {
        try {
            preparedStatement = connection.prepareStatement(CREATE_QUERY);
            preparedStatement.setString(1, entity.getLogin());
            preparedStatement.setString(2, entity.getPassword());
            preparedStatement.setString(3, entity.getPassport());
            preparedStatement.setFloat(4, entity.getMoney());

            int count = preparedStatement.executeUpdate();

            if (count > 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            logger.log(Level.ERROR, e.getMessage());
            return false;
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
    }

    /**
     * @see by.epam.bicyclesrent.dao.DAO#update(by.epam.bicyclesrent.entity.AppEntity)
     */
    @Override
    public boolean update(User entity) {
        try {
            preparedStatement = connection.prepareStatement(UPDATE_QUERY);
            preparedStatement.setString(1, entity.getLogin());
            preparedStatement.setString(2, entity.getPassword());
            preparedStatement.setString(3, entity.getPassport());
            preparedStatement.setString(4, entity.getRole().toString());
            preparedStatement.setFloat(5, entity.getMoney());
            int isBlocked = 0;
            if (entity.getIsBlocked()) {
                isBlocked = 1;
            }
            preparedStatement.setInt(6, isBlocked);
            preparedStatement.setInt(7, entity.getId());
            int count = preparedStatement.executeUpdate();
            if (count > 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            logger.log(Level.ERROR, e.getMessage());
            return false;
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
    }

}
