package by.epam.bicyclesrent.dao.mysql;

import by.epam.bicyclesrent.dao.BicycleDAO;
import by.epam.bicyclesrent.entity.Bicycle;
import by.epam.bicyclesrent.entity.BicycleType;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;

/**
 * Class BicycleDAOImpl.
 */
public class BicycleDAOImpl extends AbstractDAO implements BicycleDAO {
    /**
     * List of bicycles.
     */
    private static List<Bicycle> bicycles;
    /**
     * Item of bicycle.
     */
    private static Bicycle bicycle;

    private final String FIND_ALL_QUERY = "select b.inventory_num, b.model, b.type, p.name, "
            + "b.description, b.release_year, b.rent_price, rp.name from bicycle b join producer p "
            + "on (b.producer_id=p.id) join rent_point rp on (b.rent_point_id=rp.id);";
    private final String FIND_MOUNTAIN_QUERY = "select b.inventory_num, b.model, b.type, p.name, b.description,"
            + "b.release_year, b.rent_price, rp.name from bicycle b join producer p"
            + " on (b.producer_id=p.id) join rent_point rp on (b.rent_point_id=rp.id) where b.type='MOUNTAIN';";
    private final String FIND_URBAN_QUERY = "select b.inventory_num, b.model, b.type, p.name, "
            + "b.description, b.release_year, b.rent_price, rp.name\r\n" + "from bicycle b\r\n" + "join producer p\r\n"
            + "on (b.producer_id=p.id)\r\n" + "join rent_point rp\r\n"
            + "on (b.rent_point_id=rp.id) where b.type='URBAN';";
    private final String FIND_CARGO_QUERY = "select b.inventory_num, b.model, b.type, p.name, "
            + "b.description, b.release_year, b.rent_price, rp.name\r\n" + "from bicycle b\r\n" + "join producer p\r\n"
            + "on (b.producer_id=p.id)\r\n" + "join rent_point rp\r\n"
            + "on (b.rent_point_id=rp.id) where b.type='CARGO';";
    private final String FIND_SPORT_QUERY = "select b.inventory_num, b.model, b.type, p.name, "
            + "b.description, b.release_year, b.rent_price, rp.name\r\n" + "from bicycle b\r\n" + "join producer p\r\n"
            + "on (b.producer_id=p.id)\r\n" + "join rent_point rp\r\n"
            + "on (b.rent_point_id=rp.id) where b.type='SPORT';";
    private final String FIND_BY_ID_QUERY = "SELECT * FROM bicycle WHERE id=?;";
    private final String DEL_QUERY = "DELETE FROM bicycle where inventory_num=?;";

    /**
     * @param connection
     */
    public BicycleDAOImpl(Connection connection) {
        super(connection);
    }

    /**
     * @see by.epam.bicyclesrent.dao.AbstractDAO#findAll()
     */
    @Override
    public List<Bicycle> findAll() {
        bicycles = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(FIND_ALL_QUERY);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                bicycle = new Bicycle();
                bicycle.setInventoryNum(resultSet.getString("b.inventory_num"));
                bicycle.setModel(resultSet.getString("b.model"));
                switch (resultSet.getString("b.type")) {
                case "CARGO":
                    bicycle.setType(BicycleType.CARGO);
                    break;
                case "SPORT":
                    bicycle.setType(BicycleType.SPORT);
                    break;
                case "URBAN":
                    bicycle.setType(BicycleType.URBAN);
                    break;
                case "MOUNTAIN":
                    bicycle.setType(BicycleType.MOUNTAIN);
                    break;
                default:
                    break;
                }
                bicycle.setProducer(resultSet.getString("p.name"));
                bicycle.setDescription(resultSet.getString("b.description"));
                bicycle.setReleaseYear(Date.valueOf(resultSet.getString("b.release_year")));
                bicycle.setRentPrice(Float.valueOf(resultSet.getString("b.rent_price")));
                bicycle.setRentPoint(resultSet.getString("rp.name"));
                bicycles.add(bicycle);
            }
            return bicycles;
        } catch (SQLException e) {
            logger.log(Level.ERROR, e.getMessage());
            return bicycles;
        } finally {
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
    }

    /**
     * @see by.epam.bicyclesrent.dao.DAO#findEntityById(java.lang.Object)
     */
    @Override
    public Bicycle findEntityById(String id) {
        bicycle = new Bicycle();
        try {
            preparedStatement = connection.prepareStatement(FIND_BY_ID_QUERY);
            preparedStatement.setString(1, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                bicycle.setInventoryNum(resultSet.getString("b.inventory_num"));
                bicycle.setModel(resultSet.getString("b.model"));
                switch (resultSet.getString("b.type")) {
                case "CARGO":
                    bicycle.setType(BicycleType.CARGO);
                    break;
                case "SPORT":
                    bicycle.setType(BicycleType.SPORT);
                    break;
                case "URBAN":
                    bicycle.setType(BicycleType.URBAN);
                    break;
                case "MOUNTAIN":
                    bicycle.setType(BicycleType.MOUNTAIN);
                    break;
                default:
                    break;
                }
                bicycle.setProducer(resultSet.getString("p.name"));
                bicycle.setDescription(resultSet.getString("b.description"));
                bicycle.setReleaseYear(Date.valueOf(resultSet.getString("b.release_year")));
                bicycle.setRentPrice(Float.valueOf(resultSet.getString("b.rent_price")));
                bicycle.setRentPoint(resultSet.getString("rp.name"));
            }

            return bicycle;
        } catch (SQLException e) {
            logger.log(Level.ERROR, e.getMessage());
            return bicycle;
        } finally {
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
    }

    /**
     * @see by.epam.bicyclesrent.dao.DAO#delete(java.lang.Object)
     */
    @Override
    public boolean delete(String id) {
        try {
            preparedStatement = connection.prepareStatement(DEL_QUERY);
            preparedStatement.setString(1, id);
            int count = preparedStatement.executeUpdate();
            if (count > 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            logger.log(Level.ERROR, e.getMessage());
            return false;
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
    }

    /**
     * @see by.epam.bicyclesrent.dao.DAO#create(by.epam.bicyclesrent.entity.AppEntity)
     */
    @Override
    public boolean create(Bicycle entity) {
        int produserId = 0;
        int rentPointId = 0;
        String query1 = "SELECT id from producer where name=?;";
        String query2 = "SELECT id FROM rent_point where name=?;";
        try {
            preparedStatement = connection.prepareStatement(query1);
            preparedStatement.setString(1, entity.getProducerName());
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                produserId = Integer.parseInt(resultSet.getString("id"));
            }
            preparedStatement = connection.prepareStatement(query2);
            preparedStatement.setString(1, entity.getRentPointName());
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                rentPointId = Integer.parseInt(resultSet.getString("id"));
            }

            String query = "INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `producer_id`, `description`,"
                    + "`release_year`, `rent_price`, `rent_point_id`)" + "VALUES (?, ?, ?, ?, ?, ?,  ?, ?);";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, entity.getInventoryNum());
            preparedStatement.setString(2, entity.getModel());
            preparedStatement.setString(3, entity.getType().toString());
            preparedStatement.setInt(4, produserId);
            preparedStatement.setString(5, entity.getDescription());
            preparedStatement.setDate(6, entity.getReleaseYear());
            preparedStatement.setDouble(7, entity.getRentPrice());
            preparedStatement.setInt(8, rentPointId);
            int count = preparedStatement.executeUpdate();
            if (count > 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            logger.log(Level.ERROR, e.getMessage());
            return false;
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }

        }
    }

    /**
     * @see by.epam.bicyclesrent.dao.DAO#update(by.epam.bicyclesrent.entity.AppEntity)
     */
    @Override
    public boolean update(Bicycle entity) {
        return false;
    }

    /**
     * @see by.epam.bicyclesrent.dao.BicycleDAO#findMountain()
     */
    @Override
    public List<Bicycle> findMountain() {
        bicycles = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(FIND_MOUNTAIN_QUERY);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                bicycle = new Bicycle();
                bicycle.setInventoryNum(resultSet.getString("b.inventory_num"));
                bicycle.setModel(resultSet.getString("b.model"));
                bicycle.setType(BicycleType.MOUNTAIN);
                bicycle.setProducer(resultSet.getString("p.name"));
                bicycle.setDescription(resultSet.getString("b.description"));
                bicycle.setReleaseYear(Date.valueOf(resultSet.getString("b.release_year")));
                bicycle.setRentPrice(Float.valueOf(resultSet.getString("b.rent_price")));
                bicycle.setRentPoint(resultSet.getString("rp.name"));
                bicycles.add(bicycle);
            }
            return bicycles;
        } catch (SQLException e) {
            logger.log(Level.ERROR, e.getMessage());
            return bicycles;
        } finally {
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
    }

    /**
     * @see by.epam.bicyclesrent.dao.BicycleDAO#findUrban()
     */
    @Override
    public List<Bicycle> findUrban() {
        bicycles = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(FIND_URBAN_QUERY);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                bicycle = new Bicycle();
                bicycle.setInventoryNum(resultSet.getString("b.inventory_num"));
                bicycle.setModel(resultSet.getString("b.model"));
                bicycle.setType(BicycleType.URBAN);
                bicycle.setProducer(resultSet.getString("p.name"));
                bicycle.setDescription(resultSet.getString("b.description"));
                bicycle.setReleaseYear(Date.valueOf(resultSet.getString("b.release_year")));
                bicycle.setRentPrice(Float.valueOf(resultSet.getString("b.rent_price")));
                bicycle.setRentPoint(resultSet.getString("rp.name"));
                bicycles.add(bicycle);
            }
            return bicycles;
        } catch (SQLException e) {
            logger.log(Level.ERROR, e.getMessage());
            return bicycles;
        } finally {
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
    }

    /**
     * @see by.epam.bicyclesrent.dao.BicycleDAO#findSport()
     */
    @Override
    public List<Bicycle> findSport() {
        bicycles = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(FIND_SPORT_QUERY);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                bicycle = new Bicycle();
                bicycle.setInventoryNum(resultSet.getString("b.inventory_num"));
                bicycle.setModel(resultSet.getString("b.model"));
                bicycle.setType(BicycleType.SPORT);
                bicycle.setProducer(resultSet.getString("p.name"));
                bicycle.setDescription(resultSet.getString("b.description"));
                bicycle.setReleaseYear(Date.valueOf(resultSet.getString("b.release_year")));
                bicycle.setRentPrice(Float.valueOf(resultSet.getString("b.rent_price")));
                bicycle.setRentPoint(resultSet.getString("rp.name"));
                bicycles.add(bicycle);
            }
            return bicycles;
        } catch (SQLException e) {
            logger.log(Level.ERROR, e.getMessage());
            return bicycles;
        } finally {
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
    }

    /**
     * @see by.epam.bicyclesrent.dao.BicycleDAO#findCargo()
     */
    @Override
    public List<Bicycle> findCargo() {
        bicycles = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(FIND_CARGO_QUERY);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                bicycle = new Bicycle();
                bicycle.setInventoryNum(resultSet.getString("b.inventory_num"));
                bicycle.setModel(resultSet.getString("b.model"));
                bicycle.setType(BicycleType.CARGO);
                bicycle.setProducer(resultSet.getString("p.name"));
                bicycle.setDescription(resultSet.getString("b.description"));
                bicycle.setReleaseYear(Date.valueOf(resultSet.getString("b.release_year")));
                bicycle.setRentPrice(Float.valueOf(resultSet.getString("b.rent_price")));
                bicycle.setRentPoint(resultSet.getString("rp.name"));
                bicycles.add(bicycle);
            }
            return bicycles;
        } catch (SQLException e) {
            logger.log(Level.ERROR, e.getMessage());
            return bicycles;
        } finally {
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
    }
}
