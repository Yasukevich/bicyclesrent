package by.epam.bicyclesrent.dao.pool;

import by.epam.bicyclesrent.exceptions.PoolException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class for ConnectionPool.
 */
public class ConnectionPool {

    private static Logger logger;

    private static ConnectionPool instance = null;
    private static ReentrantLock lock = new ReentrantLock();
    private final Semaphore semaphore;
    private Queue<Connection> connections;
    ;
    private String driver;
    private String url;
    private String user;
    private String pass;
    private int maxSize;
    private int maxIdle;
    private int timeout;

    /**
     * Class constructor.
     */
    private ConnectionPool() throws PoolException {
        logger = LogManager.getLogger(ConnectionPool.class.getName());
        ResourceBundle resource = ResourceBundle.getBundle("resources.database");
        driver = resource.getString("db.driver");
        url = resource.getString("db.url");
        user = resource.getString("db.user");
        pass = resource.getString("db.password");
        maxSize = Integer.parseInt(resource.getString("db.maxActive"));
        maxIdle = Integer.parseInt(resource.getString("db.maxIdle"));
        timeout = Integer.parseInt(resource.getString("db.timeout"));
        connections = new LinkedList<>();
        semaphore = new Semaphore(maxSize, true);
        createPool();
    }

    /**
     * Method to getting instance of ConnectionPool.
     */
    public static ConnectionPool getInstance() throws PoolException {
        lock.lock();
        try {
            if (instance == null) {
                instance = new ConnectionPool();
            }
        } finally {
            lock.unlock();
        }
        return instance;
    }

    /**
     * Initialize method.
     */
    private void createPool() throws PoolException {
        try {
            Class.forName(driver);
            logger.log(Level.INFO, "Драйвер подключен");
        } catch (ClassNotFoundException e) {
            logger.log(Level.ERROR, "ERROR!DRIVER");
        }
        for (int i = 0; i < maxIdle; i++) {
            try {
                connections.add(DriverManager.getConnection(url, user, pass));
            } catch (SQLException e) {
                throw new PoolException("Error while getting a connection " + e);
            }
        }
    }

    /**
     * Method to getting Connection from the pool.
     */
    public Connection getConnection() throws PoolException {
        lock.lock();
        try {
            if (semaphore.tryAcquire(timeout, TimeUnit.MINUTES)) {
                if (connections.isEmpty()) {
                    Connection dbConnection = DriverManager.getConnection(url, user, pass);
                    logger.info("Connection to MySql has been established.");
                    connections.add(dbConnection);
                }
            }
            return connections.poll();
        } catch (SQLException | InterruptedException e) {
            throw new PoolException("Error while getting a connection " + e);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Method to returning Connection in the pool.
     */
    public void freeConnection(Connection connection) {
        lock.lock();
        try {
            connections.add(connection);
            semaphore.release();
        } finally {
            lock.unlock();
        }
    }
}
