package by.epam.bicyclesrent.dao;

import by.epam.bicyclesrent.entity.AppEntity;

import java.util.List;

/**
 * interface DAO
 */
public interface DAO<K, T extends AppEntity<K>> {

    public abstract List<T> findAll();

    public abstract T findEntityById(K id);

    public abstract boolean delete(K id);

    public abstract boolean create(T entity);

    public abstract boolean update(T entity);
}
