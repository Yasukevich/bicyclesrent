package by.epam.bicyclesrent.dao;

import by.epam.bicyclesrent.entity.User;

/**
 * interface UserDAO
 */
public interface UserDAO extends DAO<Integer, User> {

    boolean findUserByNameAndPass(String name, String enterPass);
}
