package by.epam.bicyclesrent.dao;

import by.epam.bicyclesrent.entity.Bicycle;

import java.util.List;

/**
 * interface BicycleDAO
 */
public interface BicycleDAO extends DAO<String, Bicycle> {
    @Override
    List<Bicycle> findAll();

    List<Bicycle> findMountain();

    List<Bicycle> findUrban();

    List<Bicycle> findSport();

    List<Bicycle> findCargo();
}
