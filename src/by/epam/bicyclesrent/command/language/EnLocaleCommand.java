package by.epam.bicyclesrent.command.language;

import by.epam.bicyclesrent.command.ActionCommand;
import by.epam.bicyclesrent.resource.ConfigurationManager;
import by.epam.bicyclesrent.resource.MessageManager;
import by.epam.bicyclesrent.servlet.Router;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

/**
 * Class to set English locale.
 */
public class EnLocaleCommand implements ActionCommand {
    private final static String LOCALE_VALUE = "us";
    private final static String SESSION_LOCALE_VALUE = "us-US";
    
    /** The message manager. */
    public static MessageManager messageManager = MessageManager.INSTANCE;

    /**
     * @see by.epam.bicyclesrent.command.ActionCommand#execute(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public Router execute(HttpServletRequest request) {
        Router currentRouter = new Router();
        String page = null;
        
        Locale language = new Locale(LOCALE_VALUE, LOCALE_VALUE.toUpperCase());
        messageManager.changeResource(language);

        HttpSession session = request.getSession();
        session.setAttribute("language", SESSION_LOCALE_VALUE);
        page = ConfigurationManager.getProperty("path.page.login");
        currentRouter.setPagePath(page);
        return currentRouter;
    }

}
