/**
 *
 */
package by.epam.bicyclesrent.command.users;

import by.epam.bicyclesrent.command.ActionCommand;
import by.epam.bicyclesrent.entity.User;

import java.util.List;

/**
 * @author user
 */
public abstract class UsersCommand implements ActionCommand {
    /**
     * List of users.
     */
    protected List<User> users;
}
