package by.epam.bicyclesrent.command.users;

import by.epam.bicyclesrent.logic.users.DeleteUserLogic;
import by.epam.bicyclesrent.resource.ConfigurationManager;
import by.epam.bicyclesrent.resource.MessageManager;
import by.epam.bicyclesrent.servlet.Router;
import by.epam.bicyclesrent.servlet.Router.RouterType;

import javax.servlet.http.HttpServletRequest;

/**
 *
 */
public class DelUserCommand extends UsersCommand {
    private static final String PARAM_NAME_ID = "id";

    /**
     * @see by.epam.bicyclesrent.command.ActionCommand#execute(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public Router execute(HttpServletRequest request) {
        Router currentRouter = new Router();
        String page = null;
        String id = request.getParameter(PARAM_NAME_ID);
        if (DeleteUserLogic.deleteUser(id)) {
            page = ConfigurationManager.getProperty("path.page.success");
            currentRouter.setRouter(RouterType.REDIRECT);
        } else {
            request.setAttribute("errorLoginPassMessage", MessageManager.INSTANCE.getProperty("message.exequteerror"));
            page = ConfigurationManager.getProperty("path.page.delUser");
        }
        currentRouter.setPagePath(page);
        return currentRouter;
    }

}
