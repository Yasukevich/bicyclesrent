/**
 *
 */
package by.epam.bicyclesrent.command.users;

import by.epam.bicyclesrent.logic.users.UpdateUserLogic;
import by.epam.bicyclesrent.resource.ConfigurationManager;
import by.epam.bicyclesrent.resource.MessageManager;
import by.epam.bicyclesrent.servlet.Router;
import by.epam.bicyclesrent.servlet.Router.RouterType;

import javax.servlet.http.HttpServletRequest;

/**
 * @author user
 */
public class UpdateUserCommand extends UsersCommand {
    private static final String PARAM_NAME_ID = "id";
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PARAM_NAME_PASSPORT = "passport";
    private static final String PARAM_NAME_MONEY = "money";
    private static final String PARAM_NAME_ROLE = "role";
    private static final String PARAM_NAME_IS_BLOCKED = "select";

    /**
     * @see by.epam.bicyclesrent.command.ActionCommand#execute(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public Router execute(HttpServletRequest request) {
        Router currentRouter = new Router();
        String page = null;
        String id = request.getParameter(PARAM_NAME_ID);
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String pass = request.getParameter(PARAM_NAME_PASSWORD);
        String passp = request.getParameter(PARAM_NAME_PASSPORT);
        String money = request.getParameter(PARAM_NAME_MONEY);
        String role = request.getParameter(PARAM_NAME_ROLE);
        String select = request.getParameter(PARAM_NAME_IS_BLOCKED);
        if (UpdateUserLogic.updateUser(id, login, pass, passp, money, role, select)) {
            page = ConfigurationManager.getProperty("path.page.success");
            currentRouter.setRouter(RouterType.REDIRECT);
        } else {
            request.setAttribute("errorLoginPassMessage", MessageManager.INSTANCE.getProperty("message.exequteerror"));
            page = ConfigurationManager.getProperty("path.page.updateUser");
        }
        currentRouter.setPagePath(page);
        return currentRouter;
    }

}
