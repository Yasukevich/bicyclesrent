/**
 *
 */
package by.epam.bicyclesrent.command.users;

import by.epam.bicyclesrent.logic.users.AllUsersLogic;
import by.epam.bicyclesrent.resource.ConfigurationManager;
import by.epam.bicyclesrent.servlet.Router;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * @author user
 */
public class AllUsersCommand extends UsersCommand {

    /**
     * @see by.epam.bicyclesrent.command.ActionCommand#execute(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public Router execute(HttpServletRequest request) {
        Router currentRouter = new Router();
        String page = null;
        users = new ArrayList<>();
        users = AllUsersLogic.getUsers();
        request.setAttribute("users", users);
        page = ConfigurationManager.getProperty("path.page.users");
        currentRouter.setPagePath(page);
        return currentRouter;
    }

}
