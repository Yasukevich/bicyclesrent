package by.epam.bicyclesrent.command;

import by.epam.bicyclesrent.logic.CreateAccountLogic;
import by.epam.bicyclesrent.resource.ConfigurationManager;
import by.epam.bicyclesrent.resource.MessageManager;
import by.epam.bicyclesrent.servlet.Router;
import by.epam.bicyclesrent.servlet.Router.RouterType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * class CreateAccountCommand
 */
public class CreateAccountCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PARAM_NAME_PASSPORT = "passport";
    private static final String PARAM_NAME_MONEY = "money";

    /**
     * @see by.epam.bicyclesrent.command.ActionCommand#execute(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public Router execute(HttpServletRequest request) {
        Router currentRouter = new Router();
        String page = null;
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String pass = request.getParameter(PARAM_NAME_PASSWORD);
        String passp = request.getParameter(PARAM_NAME_PASSPORT);
        String money = request.getParameter(PARAM_NAME_MONEY);
        if (CreateAccountLogic.createAccount(login, pass, passp, money)) {
            HttpSession session = request.getSession();
            session.setAttribute("user", login);
            page = ConfigurationManager.getProperty("path.page.mainR");
            currentRouter.setRouter(RouterType.REDIRECT);
        } else {
            request.setAttribute("errorLoginPassMessage", MessageManager.INSTANCE.getProperty("message.accounterror"));
            page = ConfigurationManager.getProperty("path.page.account");
            
        }
        currentRouter.setPagePath(page);
        return currentRouter;
    }

}
