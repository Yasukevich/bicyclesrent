package by.epam.bicyclesrent.command.bicycles;

import by.epam.bicyclesrent.logic.bicycles.AllBicyclesLogic;
import by.epam.bicyclesrent.resource.ConfigurationManager;
import by.epam.bicyclesrent.servlet.Router;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * class AllBicyclesCommand
 */
public class AllBicyclesCommand extends BicyclesCommand {

    @Override
    public Router execute(HttpServletRequest request) {
        Router currentRouter = new Router();
        String page = null;
        bicycles = new ArrayList<>();
        bicycles = AllBicyclesLogic.getBicycles();
        request.setAttribute("bicycles", bicycles);
        page = ConfigurationManager.getProperty("path.page.result");
        currentRouter.setPagePath(page);
        return currentRouter;
    }
}
