package by.epam.bicyclesrent.command.bicycles;

import by.epam.bicyclesrent.command.ActionCommand;
import by.epam.bicyclesrent.entity.Bicycle;

import java.util.List;

/**
 * abstract class BicyclesCommand
 */
public abstract class BicyclesCommand implements ActionCommand {
    /**
     * List of bicycles.
     */
    protected List<Bicycle> bicycles;
}
