package by.epam.bicyclesrent.command.bicycles;

import by.epam.bicyclesrent.logic.bicycles.MountainBicyclesLogic;
import by.epam.bicyclesrent.resource.ConfigurationManager;
import by.epam.bicyclesrent.resource.MessageManager;
import by.epam.bicyclesrent.servlet.Router;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * class MountainBicyclesCommand
 */
public class MountainBicyclesCommand extends BicyclesCommand {

    /**
     * @see by.epam.bicyclesrent.command.ActionCommand#execute(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public Router execute(HttpServletRequest request) {
        Router currentRouter = new Router();
        String page = null;
        bicycles = new ArrayList<>();
        bicycles = MountainBicyclesLogic.getBicycles();
        if (!bicycles.isEmpty()) {
            request.setAttribute("bicycles", bicycles);
            page = ConfigurationManager.getProperty("path.page.result");
        } else {
            request.setAttribute("errorQueryPassMessage", MessageManager.INSTANCE.getProperty("message.emptypage"));
            page = ConfigurationManager.getProperty("path.page.error");
        }
        currentRouter.setPagePath(page);
        return currentRouter;
    }
}
