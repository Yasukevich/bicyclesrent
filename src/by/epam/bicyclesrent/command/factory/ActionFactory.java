package by.epam.bicyclesrent.command.factory;

import by.epam.bicyclesrent.command.ActionCommand;
import by.epam.bicyclesrent.command.EmptyCommand;
import by.epam.bicyclesrent.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * class ActionFactory.
 */
public class ActionFactory {
    /**
     * Logger field.
     */
    protected static Logger logger;
    
    public ActionFactory() {
        logger = LogManager.getLogger(ActionFactory.class.getName());
    }

    public ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand current = new EmptyCommand();
        String action = request.getParameter("command");
        logger.log(Level.DEBUG, action);
        if (action == null || action.isEmpty()) {
            return current;
        }
        try {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            request.setAttribute("wrongAction", action + MessageManager.INSTANCE.getProperty("message.wrongaction"));
        }
        return current;
    }
}
