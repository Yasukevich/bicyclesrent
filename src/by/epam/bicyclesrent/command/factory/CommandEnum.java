package by.epam.bicyclesrent.command.factory;

import by.epam.bicyclesrent.command.ActionCommand;
import by.epam.bicyclesrent.command.CreateAccountCommand;
import by.epam.bicyclesrent.command.LoginCommand;
import by.epam.bicyclesrent.command.LogoutCommand;
import by.epam.bicyclesrent.command.bicycles.*;
import by.epam.bicyclesrent.command.language.DeLocaleCommand;
import by.epam.bicyclesrent.command.language.EnLocaleCommand;
import by.epam.bicyclesrent.command.language.RuLocaleCommand;
import by.epam.bicyclesrent.command.users.AllUsersCommand;
import by.epam.bicyclesrent.command.users.CreateUserCommand;
import by.epam.bicyclesrent.command.users.DelUserCommand;
import by.epam.bicyclesrent.command.users.UpdateUserCommand;

/**
 * enum of different commands.
 */
public enum CommandEnum {
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    CREATE_ACCOUNT {
        {
            this.command = new CreateAccountCommand();
        }
    },
    ALL_BICYCLES {
        {
            this.command = new AllBicyclesCommand();
        }
    },
    MOUNTAIN_BICYCLES {
        {
            this.command = new MountainBicyclesCommand();
        }
    },
    URBAN_BICYCLES {
        {
            this.command = new UrbanBicyclesCommand();
        }
    },
    SPORT_BICYCLES {
        {
            this.command = new SportBicyclesCommand();
        }
    },
    CARGO_BICYCLES {
        {
            this.command = new CargoBicyclesCommand();
        }
    },
    ALL_USERS {
        {
            this.command = new AllUsersCommand();
        }
    },
    CREATE_USER {
        {
            this.command = new CreateUserCommand();
        }
    },
    DEL_USER {
        {
            this.command = new DelUserCommand();
        }
    },
    UPDATE_USER {
        {
            this.command = new UpdateUserCommand();
        }
    },
    RU_LOCALE {
        {
            this.command = new RuLocaleCommand();
        }
    },
    EN_LOCALE {
        {
            this.command = new EnLocaleCommand();
        }
    },
    DE_LOCALE {
        {
            this.command = new DeLocaleCommand();
        }
    };

    ActionCommand command;

    public ActionCommand getCurrentCommand() {
        return command;
    }
}
