/**
 *
 */
package by.epam.bicyclesrent.command;

import by.epam.bicyclesrent.resource.ConfigurationManager;
import by.epam.bicyclesrent.servlet.Router;

import javax.servlet.http.HttpServletRequest;

/**
 * class EmptyCommand
 */
public class EmptyCommand implements ActionCommand {
    @Override
    public Router execute(HttpServletRequest request) {
        Router currentRouter = new Router();
        String page = ConfigurationManager.getProperty("path.page.login");
        currentRouter.setPagePath(page);
        return currentRouter;
    }
}
