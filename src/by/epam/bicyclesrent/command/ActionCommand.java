package by.epam.bicyclesrent.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.bicyclesrent.servlet.Router;

/**
 * Interface for ActionCommand.
 */
public interface ActionCommand {
    Router execute(HttpServletRequest request);
}
