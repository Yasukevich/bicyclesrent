package by.epam.bicyclesrent.command;

import by.epam.bicyclesrent.entity.Role;
import by.epam.bicyclesrent.logic.LoginLogic;
import by.epam.bicyclesrent.resource.ConfigurationManager;
import by.epam.bicyclesrent.resource.MessageManager;
import by.epam.bicyclesrent.servlet.Router;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Class for login command.
 */
public class LoginCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";

    @Override
    public Router execute(HttpServletRequest request) {
        Router currentRouter = new Router();
        String page = null;
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String pass = request.getParameter(PARAM_NAME_PASSWORD);
        if (LoginLogic.checkLogin(login, pass)) {
            HttpSession session = request.getSession();
            session.setAttribute("user", login);
            if ("root".equals(login)) {
                Role userRole = Role.ADMIN;
                session.setAttribute("userType", userRole);
                page = ConfigurationManager.getProperty("path.page.mainA");
            } else {
                Role userRole = Role.RENTER;
                session.setAttribute("userType", userRole);
                page = ConfigurationManager.getProperty("path.page.login");
            }
        } else {
            request.setAttribute("errorLoginPassMessage", MessageManager.INSTANCE.getProperty("message.loginerror"));
            page = ConfigurationManager.getProperty("path.page.loginPage");
        }

        currentRouter.setPagePath(page);
        return currentRouter;
    }
}
