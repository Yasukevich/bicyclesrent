package by.epam.bicyclesrent.servlet;

import by.epam.bicyclesrent.command.ActionCommand;
import by.epam.bicyclesrent.command.factory.ActionFactory;
import by.epam.bicyclesrent.resource.ConfigurationManager;
import by.epam.bicyclesrent.resource.MessageManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/controller")
public class BicyclesRentServlet extends HttpServlet {

    @Override
    public void init() {
        System.setProperty("log4j.configurationFile", "Log4j2.xml");
    }

    /**
     * @see javax.servlet.http.HttpServlet
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see javax.servlet.http.HttpServlet
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Router currentRouter = null;
        String page = null;
        ActionFactory client = new ActionFactory();
        ActionCommand command = client.defineCommand(request);
        currentRouter = command.execute(request);
        page = currentRouter.getPagePath();
        if (page != null) {
            switch (currentRouter.getRouter()) {
            case FORWARD:
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
                dispatcher.forward(request, response);
                break;
            case REDIRECT:
                response.sendRedirect(request.getContextPath() + page);
                break;
            }
        } else {
            page = ConfigurationManager.getProperty("path.page.login");
            request.getSession().setAttribute("nullPage", MessageManager.INSTANCE.getProperty("message.nullpage"));
            response.sendRedirect(request.getContextPath() + page);
        }
    }
}
