package by.epam.bicyclesrent.servlet;

/**
 * Class Router encapsulates the address of the page and how it is sent.
 */
public class Router {
    public enum RouterType{
        FORWARD, REDIRECT
    }
    
    private String pagePath;
    private RouterType router = RouterType.FORWARD;
    /**
     * @return the pagePath
     */
    public String getPagePath() {
        return pagePath;
    }
    /**
     * @param pagePath the pagePath to set
     */
    public void setPagePath(String pagePath) {
        this.pagePath = pagePath;
    }
    /**
     * @return the router
     */
    public RouterType getRouter() {
        return router;
    }
    /**
     * @param router the router to set
     */
    public void setRouter(RouterType router) {
        if(router == null) {
            this.router = RouterType.FORWARD;
        }
        this.router = router;
    }
}
