package by.epam.bicyclesrent.logic.users;

import by.epam.bicyclesrent.dao.mysql.UserDAOImpl;
import by.epam.bicyclesrent.dao.pool.ConnectionPool;
import by.epam.bicyclesrent.entity.Role;
import by.epam.bicyclesrent.entity.User;
import by.epam.bicyclesrent.exceptions.PoolException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

/**
 * UpdateUserLogic class
 */
public class UpdateUserLogic extends UsersLogic {
    /**
     * Logger field.
     */
    static Logger logger;

    /**
     * @param login
     * @param pass
     * @param passp
     * @param money
     * @param role
     * @param select
     * @param select2
     * @return  true if obj user was updated
     */
    public static boolean updateUser(String id, String login, String pass, String passp, String money, String role,
                                     String select) {
        boolean isUpdated = false;
        User newUser;
        logger = LogManager.getLogger(CreateUserLogic.class.getName());
        try {
            conPool = ConnectionPool.getInstance();
            dbConnection = conPool.getConnection();
            dbConnection.setAutoCommit(false);
            UserDAOImpl userDAOImpl = new UserDAOImpl(dbConnection);
            int userId = Integer.parseInt(id);
            newUser = userDAOImpl.findEntityById(userId);
            if (login.length() > 0) {
                newUser.setLogin(login);
            }
            if (pass.length() > 0) {
                newUser.setPassword(pass);
            }
            if (passp.length() > 0) {
                newUser.setPassport(passp);
            }
            if (money.length() > 0) {
                float userMoney = Float.parseFloat(money);
                newUser.setMoney(userMoney);
            }
            if (role.length() > 0) {
                Role userRole = null;
                switch (role) {
                    case "RENTER":
                        userRole = Role.RENTER;
                        break;
                    case "ADMIN":
                        userRole = Role.ADMIN;
                        break;
                }
                newUser.setRole(userRole);
            }
            if (select.length() > 0) {
                boolean isBlocked = Boolean.parseBoolean(select);
                newUser.setBlocked(isBlocked);
            }
            isUpdated = userDAOImpl.update(newUser);
            dbConnection.commit();
            conPool.freeConnection(dbConnection);
        } catch (SQLException | PoolException e) {
            logger.log(Level.ERROR, e.getMessage());
        }
        return isUpdated;
    }
}
