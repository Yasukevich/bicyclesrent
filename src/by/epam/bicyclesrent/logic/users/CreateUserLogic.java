package by.epam.bicyclesrent.logic.users;

import by.epam.bicyclesrent.dao.mysql.UserDAOImpl;
import by.epam.bicyclesrent.dao.pool.ConnectionPool;
import by.epam.bicyclesrent.entity.Role;
import by.epam.bicyclesrent.entity.User;
import by.epam.bicyclesrent.exceptions.PoolException;
import by.epam.bicyclesrent.validator.UserValidator;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

/**
 * CreateUserLogic class
 */
public class CreateUserLogic extends UsersLogic {
    /**
     * Logger field.
     */
    static Logger logger;

    /**
     * @param enterLogin is user's login
     * @param enterPass is user's password
     * @param enterPassport is user's passport
     * @param enterMoney is user's money
     * @param role is user's role
     * @param select is user blocked or not
     * @return true if obj user was created
     */
    public static boolean createUser(String enterLogin, String enterPass, String enterPassport, String enterMoney,
            String role, String select) {
        boolean isCreated = false;
        if (UserValidator.validateLogin(enterLogin) && UserValidator.validatePassword(enterPass)
                && UserValidator.validatePassport(enterPassport) && UserValidator.validateMoney(enterMoney)) {
            User newUser;
            logger = LogManager.getLogger(CreateUserLogic.class.getName());
            try {
                conPool = ConnectionPool.getInstance();
                dbConnection = conPool.getConnection();
                dbConnection.setAutoCommit(false);
                UserDAOImpl userDAOImpl = new UserDAOImpl(dbConnection);
                Role userRole = null;
                switch (role) {
                case "RENTER":
                    userRole = Role.RENTER;
                    break;
                case "ADMIN":
                    userRole = Role.ADMIN;
                    break;
                }
                boolean isBlocked = Boolean.parseBoolean(select);
                float userMoney = Float.parseFloat(enterMoney);
                newUser = new User(enterLogin, enterPass, userRole, enterPassport, userMoney, isBlocked);
                isCreated = userDAOImpl.create(newUser);
                dbConnection.commit();
                conPool.freeConnection(dbConnection);
            } catch (SQLException | PoolException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
        return isCreated;
    }
}
