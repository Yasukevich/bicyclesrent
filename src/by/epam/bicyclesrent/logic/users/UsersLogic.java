package by.epam.bicyclesrent.logic.users;

import by.epam.bicyclesrent.entity.User;
import by.epam.bicyclesrent.logic.Logic;

import java.util.List;

/**
 * abstract class UsersLogic
 */
public abstract class UsersLogic extends Logic {
    /**
     * List of users.
     */
    protected static List<User> users;
}
