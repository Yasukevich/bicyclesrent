package by.epam.bicyclesrent.logic.users;

import by.epam.bicyclesrent.dao.mysql.UserDAOImpl;
import by.epam.bicyclesrent.dao.pool.ConnectionPool;
import by.epam.bicyclesrent.entity.User;
import by.epam.bicyclesrent.exceptions.PoolException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * AllUsersLogic class
 */
public class AllUsersLogic extends UsersLogic {
    /**
     * Logger field.
     */
    static Logger logger;

    /**
     * @return  List of Users
     */
    public static List<User> getUsers() {
        logger = LogManager.getLogger(AllUsersLogic.class.getName());
        try {
            conPool = ConnectionPool.getInstance();
            dbConnection = conPool.getConnection();
            dbConnection.setAutoCommit(false);
            users = new ArrayList<>();
            UserDAOImpl userDAOImpl = new UserDAOImpl(dbConnection);
            users = userDAOImpl.findAll();
            dbConnection.commit();
            conPool.freeConnection(dbConnection);
        } catch (SQLException | PoolException e) {
            logger.log(Level.ERROR, e.getMessage());
        }
        return users;
    }
}
