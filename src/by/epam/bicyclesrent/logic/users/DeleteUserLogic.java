package by.epam.bicyclesrent.logic.users;

import by.epam.bicyclesrent.dao.mysql.UserDAOImpl;
import by.epam.bicyclesrent.dao.pool.ConnectionPool;
import by.epam.bicyclesrent.exceptions.PoolException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

/**
 * DeleteUserLogic class
 */
public class DeleteUserLogic extends UsersLogic {
    /**
     * Logger field.
     */
    static Logger logger;

    /**
     * @param id
     * @return true if entity was created
     */
    public static boolean deleteUser(String id) {
        boolean isDeleted = false;
        logger = LogManager.getLogger(DeleteUserLogic.class.getName());
        try {
            conPool = ConnectionPool.getInstance();
            dbConnection = conPool.getConnection();
            dbConnection.setAutoCommit(false);
            UserDAOImpl userDAOImpl = new UserDAOImpl(dbConnection);
            int userId = Integer.parseInt(id);
            isDeleted = userDAOImpl.delete(userId);
            dbConnection.commit();
            conPool.freeConnection(dbConnection);
        } catch (SQLException | PoolException e) {
            logger.log(Level.ERROR, e.getMessage());
        }
        return isDeleted;
    }
}
