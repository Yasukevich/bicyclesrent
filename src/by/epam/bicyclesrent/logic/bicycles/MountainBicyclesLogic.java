package by.epam.bicyclesrent.logic.bicycles;

import by.epam.bicyclesrent.dao.mysql.BicycleDAOImpl;
import by.epam.bicyclesrent.dao.pool.ConnectionPool;
import by.epam.bicyclesrent.entity.Bicycle;
import by.epam.bicyclesrent.exceptions.PoolException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * MountainBicyclesLogic class
 */
public class MountainBicyclesLogic extends BicyclesLogic {
    /**
     * Logger field.
     */
    static Logger logger;

    /**
     * @return List of Bicycles
     */
    public static List<Bicycle> getBicycles() {
        logger = LogManager.getLogger(MountainBicyclesLogic.class.getName());
        try {
            conPool = ConnectionPool.getInstance();
            dbConnection = conPool.getConnection();
            dbConnection.setAutoCommit(false);
            bicycles = new ArrayList<>();
            BicycleDAOImpl bicycleDAOImpl = new BicycleDAOImpl(dbConnection);
            bicycles = bicycleDAOImpl.findMountain();
            dbConnection.commit();
            conPool.freeConnection(dbConnection);
        } catch (SQLException | PoolException e) {
            logger.log(Level.ERROR, e.getMessage());
        }
        return bicycles;
    }
}
