package by.epam.bicyclesrent.logic.bicycles;

import by.epam.bicyclesrent.entity.Bicycle;
import by.epam.bicyclesrent.logic.Logic;

import java.util.List;

/**
 * abstract class BicyclesLogic
 */
public abstract class BicyclesLogic extends Logic {
    /**
     * List of bicycles.
     */
    protected static List<Bicycle> bicycles;
}
