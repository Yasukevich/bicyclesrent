package by.epam.bicyclesrent.logic;

import by.epam.bicyclesrent.dao.pool.ConnectionPool;

import java.sql.Connection;

/**
 * abstract class Logic
 */
public abstract class Logic {
    protected static ConnectionPool conPool;
    /**
     * DB connection field.
     */
    protected static Connection dbConnection;
}
