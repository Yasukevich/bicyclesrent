package by.epam.bicyclesrent.logic;

import by.epam.bicyclesrent.dao.mysql.UserDAOImpl;
import by.epam.bicyclesrent.dao.pool.ConnectionPool;
import by.epam.bicyclesrent.exceptions.PoolException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

/**
 * LoginLogic class
 */
public class LoginLogic extends Logic {
    /**
     * Logger field.
     */
    static Logger logger;

    public static boolean checkLogin(String enterLogin, String enterPass) {
        boolean isExist = false;
        if (enterLogin.length() != 0 && enterPass.length() != 0) {
            logger = LogManager.getLogger(LoginLogic.class.getName());
            try {
                conPool = ConnectionPool.getInstance();
                dbConnection = conPool.getConnection();
                dbConnection.setAutoCommit(false);
                UserDAOImpl userDAOImpl = new UserDAOImpl(dbConnection);
                isExist = userDAOImpl.findUserByNameAndPass(enterLogin, enterPass);
                dbConnection.commit();
                conPool.freeConnection(dbConnection);
            } catch (SQLException | PoolException e) {
                logger.log(Level.ERROR, e.getMessage());
            }
        }
        return isExist;
    }
}
