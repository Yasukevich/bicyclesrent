package by.epam.bicyclesrent.logic;

import by.epam.bicyclesrent.dao.mysql.UserDAOImpl;
import by.epam.bicyclesrent.dao.pool.ConnectionPool;
import by.epam.bicyclesrent.entity.Role;
import by.epam.bicyclesrent.entity.User;
import by.epam.bicyclesrent.exceptions.PoolException;
import by.epam.bicyclesrent.validator.UserValidator;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

/**
 * Сlass CreateAccountLogic.
 */
public class CreateAccountLogic extends Logic {
    /**
     * Logger field.
     */
    static Logger logger;

    public static boolean createAccount(String enterLogin, String enterPass, String enterPassport, String enterMoney) {
        boolean isCreated = false;
        if (UserValidator.validateLogin(enterLogin) && UserValidator.validatePassword(enterPass)
                && UserValidator.validatePassport(enterPassport) && UserValidator.validateMoney(enterMoney)) {
            if (enterLogin.length() != 0 && enterPass.length() != 0 && enterPassport.length() != 0) {
                User newUser;
                logger = LogManager.getLogger(CreateAccountLogic.class.getName());
                try {
                    conPool = ConnectionPool.getInstance();
                    dbConnection = conPool.getConnection();
                    dbConnection.setAutoCommit(false);
                    UserDAOImpl userDAOImpl = new UserDAOImpl(dbConnection);
                    float userMoney = Float.parseFloat(enterMoney);
                    newUser = new User(enterLogin, enterPass, Role.RENTER, enterPassport, userMoney, true);
                    isCreated = userDAOImpl.create(newUser);
                    dbConnection.commit();
                    conPool.freeConnection(dbConnection);
                } catch (SQLException | PoolException e) {
                    logger.log(Level.ERROR, e.getMessage());
                }
            }
        }
        return isCreated;
    }
}
