package by.epam.bicyclesrent.validator;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class for validate entity user.
 */
public class UserValidator {
    /**
     * Logger field.
     */
    protected static Logger logger = LogManager.getLogger(UserValidator.class.getName());
    /**
     * constant - regular expression for checking login.
     */
    static final String RIGHT_LOGIN_EXPRESSION = "([A-Za-zА-Яа-я0-9_-]){3,16}";
    /**
     * constant - regular expression for checking password.
     */
    static final String RIGHT_PASSWORD_EXPRESSION = "([0-9]){1,16}";
    /**
     * constant - regular expression for checking passport.
     */
    static final String RIGHT_PASSPORT_EXPRESSION = "([A-Za-zА-Яа-я0-9]){9}";
    /**
     * constant for min amount of user's money.
     */
    private final static float MONEY_MIN = 0;
    /**
     * constant for max amount of user's money.
     */
    private final static float MONEY_MAX = 100_000;
    
    /**
     * Validate login.
     *
     * @param enterLogin the enter login
     * @return true, if successful
     */
    public static boolean validateLogin(String enterLogin) {
        logger.log(Level.INFO, "Validate Login: " + enterLogin.matches(RIGHT_LOGIN_EXPRESSION));
        return enterLogin.matches(RIGHT_LOGIN_EXPRESSION);
    }

    /**
     * Validate password.
     *
     * @param enterPass the enter pass
     * @return true, if successful
     */
    public static boolean validatePassword(String enterPass) {
        logger.log(Level.INFO, "Validate password: " + enterPass.matches(RIGHT_LOGIN_EXPRESSION));
        return enterPass.matches(RIGHT_LOGIN_EXPRESSION);
    }
    
    /**
     * Validate passport.
     *
     * @param enterPassp the enter passport
     * @return true, if successful
     */
    public static boolean validatePassport(String enterPassp) {
        logger.log(Level.INFO, "Validate passport: " + enterPassp.matches(RIGHT_PASSPORT_EXPRESSION));
        return enterPassp.matches(RIGHT_PASSPORT_EXPRESSION);
    }
    
    /**
     * Validate money.
     *
     * @param enterMoney the enter money
     * @return true, if successful
     */
    public static boolean validateMoney(String enterMoney) {
        if (enterMoney.isEmpty()){
            return false;
        }
        else {
            float money = Float.parseFloat(enterMoney);
            logger.log(Level.INFO, "Validate money: " + (money > MONEY_MIN && money < MONEY_MAX));
            return money > MONEY_MIN && money < MONEY_MAX;
        }
    }
}
