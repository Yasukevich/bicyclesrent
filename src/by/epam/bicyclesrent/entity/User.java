package by.epam.bicyclesrent.entity;

/**
 * AppEntity class user.
 */
public class User extends AppEntity<Integer> {
    /**
     * User's login.
     */
    private String login;
    /**
     * User's password.
     */
    private String password;
    /**
     * User's role.
     */
    private Role role;
    /**
     * User's passport.
     */
    private String passport;
    /**
     * User's money.
     */
    private float money;
    /**
     * It's blocked user or not.
     */
    private boolean isBlocked;

    /**
     * Class constructor without arguments.
     */
    public User() {
    }

    /**
     * @param login     to set login
     * @param password  to set password
     * @param role      to set role
     * @param passport  to set passport
     * @param money     to set money
     * @param isBlocked to set isBlocked
     */
    public User(String login, String password, Role role, String passport, float money, boolean isBlocked) {
        this.login = login;
        this.password = password;
        this.role = role;
        this.passport = passport;
        this.money = money;
        this.isBlocked = isBlocked;
    }

    /**
     * @param login     to set login
     * @param password  to set password
     * @param role      to set role
     * @param passport  to set passport
     * @param isBlocked to set isBlocked
     */
    public User(String login, String password, Role role, String passport, boolean isBlocked) {
        this.login = login;
        this.password = password;
        this.role = role;
        this.passport = passport;
        this.isBlocked = isBlocked;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * @return the passport
     */
    public String getPassport() {
        return passport;
    }

    /**
     * @param passport the passport to set
     */
    public void setPassport(String passport) {
        this.passport = passport;
    }

    /**
     * @return the money
     */
    public float getMoney() {
        return money;
    }

    /**
     * @param money the money to set
     */
    public void setMoney(float money) {
        this.money = money;
    }

    /**
     * @return the isBlocked
     */
    public boolean getIsBlocked() {
        return isBlocked;
    }

    /**
     * @param isBlocked the isBlocked to set
     */
    public void setBlocked(boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "User [login=" + login + ", password=" + password + ", role=" + role + ", passport=" + passport
                + ", money=" + money + ", isBlocked=" + isBlocked + ", id=" + id + "]";
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (isBlocked ? 1231 : 1237);
        result = prime * result + ((login == null) ? 0 : login.hashCode());
        result = prime * result + Float.floatToIntBits(money);
        result = prime * result + ((passport == null) ? 0 : passport.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((role == null) ? 0 : role.hashCode());
        return result;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (isBlocked != other.isBlocked)
            return false;
        if (login == null) {
            if (other.login != null)
                return false;
        } else if (!login.equals(other.login))
            return false;
        if (Float.floatToIntBits(money) != Float.floatToIntBits(other.money))
            return false;
        if (passport == null) {
            if (other.passport != null)
                return false;
        } else if (!passport.equals(other.passport))
            return false;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        if (role != other.role)
            return false;
        return true;
    }
}
