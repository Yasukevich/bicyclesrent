/**
 *
 */
package by.epam.bicyclesrent.entity;

/**
 * @author user
 */
public enum Role {
    /**
     * User's role admin.
     */
    ADMIN,
    /**
     * User's role renter.
     */
    RENTER,
    /**
     * User's role moderator.
     */
    MODERATOR
}
