package by.epam.bicyclesrent.entity;

/**
 * Enumeration of bicycle's type.
 */
public enum BicycleType {
    /**
     * Constant for sport bicycle.
     */
    SPORT,
    /**
     * Constant for cargo bicycle.
     */
    CARGO,
    /**
     * Constant for urban bicycle.
     */
    URBAN,
    /**
     * Constant for mountain bicycle.
     */
    MOUNTAIN
}
