/**
 *
 */
package by.epam.bicyclesrent.entity;

/**
 * @author user
 */
public abstract class AppEntity<K> {
    /**
     * Entity's id.
     */
    protected K id;

    /**
     * @return the id
     */
    public K getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(K id) {
        this.id = id;
    }
}
