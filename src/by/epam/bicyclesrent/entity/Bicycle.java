package by.epam.bicyclesrent.entity;

import java.sql.Date;

/**
 * AppEntity class bicycle.
 */
public class Bicycle extends AppEntity<String> {
    /**
     * Inventory number of bicycle.
     */
    private String inventoryNum;
    /**
     * Model of bicycle.
     */
    private String model;
    /**
     * Type of bicycle.
     */
    private BicycleType type;
    /**
     * Producer of bicycle.
     */
    private String producerName;
    /**
     * Release year of bicycle.
     */
    private Date releaseYear;
    /**
     * Rent price of bicycle.
     */
    private float rentPrice;
    /**
     * Rent point of bicycle.
     */
    private String rentPointName;

    /**
     * Description of bicycle.
     */
    private String description;
    /**
     * Image of bicycle.
     */
    private String image;


    /**
     * Class constructor without arguments.
     */
    public Bicycle() {
    }

    /**
     * @param inventoryNum
     * @param model
     * @param type
     * @param producerName
     * @param description
     * @param releaseYear
     * @param rentPointName
     */
    public Bicycle(String inventoryNum, String model, BicycleType type, String producerName,
                   Date releaseYear, String rentPointName, String description) {
        this.inventoryNum = inventoryNum;
        this.model = model;
        this.type = type;
        this.producerName = producerName;
        this.description = description;
        this.releaseYear = releaseYear;
        this.rentPointName = rentPointName;
    }

    /**
     * @param inventoryNum
     * @param model
     * @param type
     * @param producerName
     * @param description
     * @param releaseYear
     * @param rentPrice
     * @param rentPointName
     * @param image
     */
    public Bicycle(String inventoryNum, String model, BicycleType type, String producerName,
                   Date releaseYear, float rentPrice, String rentPointName, String description) {
        this.inventoryNum = inventoryNum;
        this.model = model;
        this.type = type;
        this.producerName = producerName;
        this.description = description;
        this.releaseYear = releaseYear;
        this.rentPrice = rentPrice;
        this.rentPointName = rentPointName;
    }

    /**
     * @param producer the producer to set
     */
    public void setProducer(String producer) {
        this.producerName = producer;
    }

    /**
     * @param rentPoint the rentPoint to set
     */
    public void setRentPoint(String rentPoint) {
        this.rentPointName = rentPoint;
    }

    /**
     * @return the producerName
     */
    public String getProducerName() {
        return producerName;
    }

    /**
     * @param producerName the producerName to set
     */
    public void setProducerName(String producerName) {
        this.producerName = producerName;
    }

    /**
     * @return the rentPointName
     */
    public String getRentPointName() {
        return rentPointName;
    }

    /**
     * @param rentPointName the rentPointName to set
     */
    public void setRentPointName(String rentPointName) {
        this.rentPointName = rentPointName;
    }

    /**
     * @return the inventoryNum
     */
    public String getInventoryNum() {
        return inventoryNum;
    }

    /**
     * @param inventoryNum the inventoryNum to set
     */
    public void setInventoryNum(String inventoryNum) {
        this.inventoryNum = inventoryNum;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the type
     */
    public BicycleType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(BicycleType type) {
        this.type = type;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the releaseYear
     */
    public Date getReleaseYear() {
        return releaseYear;
    }

    /**
     * @param releaseYear the releaseYear to set
     */
    public void setReleaseYear(Date releaseYear) {
        this.releaseYear = releaseYear;
    }

    /**
     * @return the rentPrice
     */
    public float getRentPrice() {
        return rentPrice;
    }

    /**
     * @param rentPrice the rentPrice to set
     */
    public void setRentPrice(float rentPrice) {
        this.rentPrice = rentPrice;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Bicycle [inventoryNum=" + inventoryNum + ", model=" + model + ", type=" + type + ", producerName="
                + producerName + ", releaseYear=" + releaseYear + ", rentPrice="
                + rentPrice + ", rentPointName=" + rentPointName + "]";
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((image == null) ? 0 : image.hashCode());
        result = prime * result + ((inventoryNum == null) ? 0 : inventoryNum.hashCode());
        result = prime * result + ((model == null) ? 0 : model.hashCode());
        result = prime * result + ((producerName == null) ? 0 : producerName.hashCode());
        result = prime * result + ((releaseYear == null) ? 0 : releaseYear.hashCode());
        result = prime * result + ((rentPointName == null) ? 0 : rentPointName.hashCode());
        result = prime * result + Float.floatToIntBits(rentPrice);
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Bicycle other = (Bicycle) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (image == null) {
            if (other.image != null)
                return false;
        } else if (!image.equals(other.image))
            return false;
        if (inventoryNum == null) {
            if (other.inventoryNum != null)
                return false;
        } else if (!inventoryNum.equals(other.inventoryNum))
            return false;
        if (model == null) {
            if (other.model != null)
                return false;
        } else if (!model.equals(other.model))
            return false;
        if (producerName == null) {
            if (other.producerName != null)
                return false;
        } else if (!producerName.equals(other.producerName))
            return false;
        if (releaseYear == null) {
            if (other.releaseYear != null)
                return false;
        } else if (!releaseYear.equals(other.releaseYear))
            return false;
        if (rentPointName == null) {
            if (other.rentPointName != null)
                return false;
        } else if (!rentPointName.equals(other.rentPointName))
            return false;
        if (Float.floatToIntBits(rentPrice) != Float.floatToIntBits(other.rentPrice))
            return false;
        if (type != other.type)
            return false;
        return true;
    }

}
