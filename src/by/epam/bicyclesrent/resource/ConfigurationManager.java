package by.epam.bicyclesrent.resource;

import java.util.ResourceBundle;

/**
 * class ConfigurationManager to get page's path from property file. 
 */
public class ConfigurationManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.config");

    private ConfigurationManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
