package by.epam.bicyclesrent.resource;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * The Enum ResourceManager.
 */
public enum ResourceManager {
    /** The instance. */
    INSTANCE;
    /** The resource name. */
    private final String resourceName = "resources.pagecontent";
    /** The resource bundle. */
    private ResourceBundle resourceBundle;

    /**
     * Instantiates a new resource manager.
     */
    private ResourceManager() {
        resourceBundle = ResourceBundle.getBundle(resourceName, Locale.getDefault());
    }

    /**
     * Change resource.
     *
     * @param locale the locale
     */
    public void changeResource(Locale locale) {
        resourceBundle = ResourceBundle.getBundle(resourceName, locale);
    }


    /**
     * @return the current resourceBundle
     */
    public ResourceBundle getResourceBundle() {
        return resourceBundle;
    }
    
    /**
    * Gets the property.
    *
    * @param key the key
    * @return the property
    */
    public String getString(String key) {
        return resourceBundle.getString(key);
    }
}
