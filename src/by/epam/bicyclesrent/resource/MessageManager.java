package by.epam.bicyclesrent.resource;

import java.util.ResourceBundle;
import java.util.Locale;

/**
 * enum MessageManager to get messages from property file.
 */
public enum  MessageManager {
    
    /** The instance. */
    INSTANCE;
    
    /** The message manager. */
    private ResourceBundle resourceBundle;
    
    /** The resource name. */
    private final String RESOURCE_NAME = "resources.messages";
    
    /**
     * Instantiates a new message manager.
     */
    private MessageManager() {
        resourceBundle = ResourceBundle.getBundle(RESOURCE_NAME, Locale.getDefault());
    }
    
    /**
     * Change resource.
     *
     * @param locale the locale
     */
    public void changeResource(Locale locale) {
        resourceBundle = ResourceBundle.getBundle(RESOURCE_NAME, locale);
    }
    
    /**
     * Gets the property.
     *
     * @param key the key
     * @return the property
     */
    public String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
