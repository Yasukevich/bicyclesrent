/**
 * Package for service classes that retrieve from the properties
 * files necessary for the function
 * application information.
 */
package by.epam.bicyclesrent.resource;