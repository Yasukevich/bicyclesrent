/**
 * Exception package of the app BicyclesRent
 *
 * @author YasAnn
 */
package by.epam.bicyclesrent.exceptions;