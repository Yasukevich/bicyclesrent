package by.epam.bicyclesrent.exceptions;

/**
 * The class that indicates exception.
 */
public class PoolException extends Exception {

    /**
     * Constructor of PoolException
     * with the specified detail message.
     *
     * @param textMessag the detail message.
     */
    public PoolException(String textMessag) {
        super(textMessag);
    }
}
