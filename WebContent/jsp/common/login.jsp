<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
    value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
    scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="resources.pagecontent" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login page</title>
<link rel="stylesheet" type="text/css" href="../../css/main.css">
</head>
<body>
<c:import url="header.jsp"/>
	<form name="loginForm" action="${ pageContext.request.contextPath }/controller" method="POST">
		<input type="hidden" name="command" value="login" /> Login:<br /> <input
			type="text" name="login" value="" pattern="([A-Za-zА-Яа-я0-9_-]){3,16}" required /> <br /> Password:<br />
		<input type="password" name="password" value="" placeholder="Только цифры" pattern="([0-9]){1,16}" required /> <br />
		${errorLoginPassMessage} <br /> ${wrongAction} <br /> ${nullPage} <br />
		<input type="submit" value="Log in" />

	</form>
</body>
</html>