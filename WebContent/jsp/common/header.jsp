<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="resources.pagecontent"/>
<head>
    <title>Header</title>
    <link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/css/style.css">
</head>
<body>
<header>                      

    <div id="localization">
        <a href="controller?command=en_locale">en</a><span> |</span> <a
            href="controller?command=ru_locale">ru</a><span> |</span> <a
            href="controller?command=de_locale">de</a>
    </div>
    <br>
    <a href="${ pageContext.request.contextPath }"><img src="${ pageContext.request.contextPath }/img/logo2.jpg"
                                  alt="Bicycles rent" title="Home"></a>

    <div id="twoforms">
        <ctg:hello role="${ user }"/>
        <c:if test="${ empty user }">

            <a href="${ pageContext.request.contextPath }\jsp\common\login.jsp"><fmt:message key="label.login"
                                                        /></a>
            <a href="${ pageContext.request.contextPath }\jsp\renter\account.jsp"><fmt:message key="label.account"
                                                          /></a>
        </c:if>


        <c:if test="${ not empty user }">
            <hr/>
            <a href="${ pageContext.request.contextPath }/controller?command=logout"><fmt:message
                    key="label.logout"/></a>
        </c:if>

        <hr/>
    </div>

    <div id="heading">
        <h1>
            <fmt:message key="label.about"/>
        </h1>
    </div>
    <nav>
        <ul class="top-menu">
            <li><a href="${ pageContext.request.contextPath }"><fmt:message key="label.home" /></a></li>
            <li class="active"><fmt:message key="label.about" /></li>
            <li><a href="${ pageContext.request.contextPath }/jsp/admin/main.jsp"><fmt:message key="label.admin" /></a></li>
            <li><a href="${ pageContext.request.contextPath }"><fmt:message key="label.rent_points" /></a></li>
            <li><a href="${ pageContext.request.contextPath }"><fmt:message key="label.produsers" /></a></li>
            <li><a href="${ pageContext.request.contextPath }"><fmt:message key="label.countries" /></a></li>
            <li><a href="${ pageContext.request.contextPath }"><fmt:message key="label.orders" /></a></li>
        </ul>
    </nav>


</header>
</body>
</html>