<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Footer</title>
</head>
<body>
<footer>
    <hr>
    <div id="copyright">
        <hr>
        <jsp:useBean id="now" class="java.util.Date"/>
        <fmt:setLocale value="en-EN"/>
        <p>Copyright © <fmt:formatDate value="${now}"/> ${ user }</p>
        <br/>
    </div>
</footer>
</body>
</html>