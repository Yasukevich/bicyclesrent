<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
    value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
    scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="resources.pagecontent" />
<html>
<head>
    <title>ParsingResult</title>
    <link rel="stylesheet" type="text/css"
          href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css"
          href="bootstrap-3.3.7-dist/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

<c:import url="./common/header.jsp"/>

<table border="1" class="table  table-striped">
    <tr>
        <th>Number</th>
        <th>Model</th>
        <th>type</th>
        <th>producer</th>
        <th>releaseYear</th>
        <th>rentPrice</th>
    </tr>
    <c:forEach var="elem" items="${bicycles}" varStatus="loop">
        <tr>
            <td><c:out value="${ loop.index+1 }"/></td>
            <td><c:out value="${ elem.model }"/></td>
            <td><c:out value="${ elem.type }"/></td>
            <td><c:out value="${ elem.producerName }"/></td>
            <td><c:out value="${ elem.releaseYear }"/></td>
            <td><c:out value="${ elem.rentPrice }"/></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>