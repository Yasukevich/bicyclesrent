<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
    value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
    scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="resources.pagecontent" />
<html>
<head>
    <title>AllUsers</title>
    <link rel="stylesheet" type="text/css"
          href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css"
          href="bootstrap-3.3.7-dist/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
<hr/>
<c:import url="..\common\header.jsp"/>
<hr/>
<table border="1" class="table  table-striped">
    <tr>
        <th>Number</th>
        <th>Login</th>
        <th>password</th>
        <th>passport</th>
        <th>role</th>
        <th>money</th>
        <th>isBlocked</th>
    </tr>
    <c:forEach var="elem" items="${users}" varStatus="loop">
        <tr>
            <td><c:out value="${ loop.index+1 }"/></td>
            <td><c:out value="${ elem.login }"/></td>
            <td><c:out value="${ elem.password }"/></td>
            <td><c:out value="${ elem.passport }"/></td>
            <td><c:out value="${ elem.role }"/></td>
            <td><c:out value="${ elem.money }"/></td>
            <td><c:out value="${ elem.isBlocked }"/></td>
        </tr>
    </c:forEach>
</table>
<c:import url="..\common\footer.jsp"/>
</body>
</html>