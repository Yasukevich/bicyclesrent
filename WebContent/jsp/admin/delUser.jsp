<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
    value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
    scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="resources.pagecontent" />
<html>
<head>
    <title>Delete user</title>
    <link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/css/main.css">
</head>
<body>
<c:import url="..\common\header.jsp"/>
<h1>Delete user by id</h1>
<div>
    <form name="deleteUserForm" action="../../controller" method="POST">
        <input type="hidden" name="command" value="del_user"/>
        Enter user's id:<br/>
        <input type="text" name="id" value="" required/> <br/>
        ${errorLoginPassMessage} <br/> ${wrongAction} <br/> ${nullPage} <br/>
        <input type="submit" value="Delete"/>
    </form>
</div>
<c:import url="..\common\footer.jsp"/>
</body>
</html>