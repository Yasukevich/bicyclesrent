<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
    value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
    scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="resources.pagecontent" />
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Success!!!</title>
</head>
<body>
<c:import url="..\common\header.jsp"/>
<p>Operation was successfully completed</p>
</body>
</html>