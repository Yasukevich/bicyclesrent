<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
    value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
    scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="resources.pagecontent" />
<html>
<html>
<head>
    <title>BICYCLESRENT.BY - прокат ВЕЛОСИПЕДОВ в Минске</title>
    <link rel="stylesheet" type="text/css"
          href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css"
          href="bootstrap-3.3.7-dist/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/css/style.css">
</head>
<body>
<%-- <c:set var="admin" value="root" scope="session"/> --%>
<header>
    <c:import url="..\common\header.jsp"/>

</header>
<table border="1" class="table  table-striped">
    <tr>
        <th width="25%">Bicycles</th>
        <th width="25%">Users</th>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td><a href="${ pageContext.request.contextPath }/controller?command=all_bicycles">All
                        bicycles</a></td>
                </tr>
                <tr>
                    <td><a href="${ pageContext.request.contextPath }/controller?command=cargo_bicycles">Cargo bicycles</a></td>
                </tr>
                <tr>
                    <td><a href="${ pageContext.request.contextPath }/controller?command=mountain_bicycles">Mountain
                        bicycles</a></td>
                </tr>
                <tr>
                    <td><a href="${ pageContext.request.contextPath }/controller?command=sport_bicycles">Sport
                        bicycles</a></td>
                </tr>
                <tr>
                    <td><a href="${ pageContext.request.contextPath }/controller?command=urban_bicycles">Urban
                        bicycles</a></td>
                </tr>
            </table>
        </td>
        <td>
            <table>
                <tr>
                    <td><a href="${ pageContext.request.contextPath }/controller?command=all_users">All
                        users</a></td>
                </tr>
                <tr>
                    <td><a href="${ pageContext.request.contextPath }/jsp/admin/newUser.jsp">Add
                        user</a></td>
                </tr>
                <tr>
                    <td><a href="${ pageContext.request.contextPath }/jsp/admin/updateUser.jsp">Update
                        user</a></td>
                </tr>
                <tr>
                    <td><a href="${ pageContext.request.contextPath }/jsp/admin/delUser.jsp">Delete
                        user</a></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<c:import url="..\common\footer.jsp"/>
</body>
</html>