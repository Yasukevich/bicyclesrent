<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
    value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
    scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="resources.pagecontent" />
<html>
<head>
<title>Create new user</title>
<link rel="stylesheet" type="text/css" href="../../css/main.css">
</head>
<body>
	<c:import url="..\common\header.jsp"/>
	<br>
	<h1>Add new user in DB</h1>
	<div>
		<form name="createUserForm" action="${ pageContext.request.contextPath }/controller" method="POST">
			<input type="hidden" name="command" value="create_user" />
			Login:<br />
			<input type="text" name="login" value=""
				pattern="([A-Za-zА-Яа-я0-9_-]){3,16}" required /> <br />
			Password:<br />
			<input type="password" name="password" value=""
				placeholder="Только цифры" pattern="([0-9]){1,16}" required /> <br />
			Passport's number<br /> <input type="text" name="passport" value=""
				pattern="([A-Za-zА-Яа-я0-9]){9}" required /> <br />
			Money<br /> <input type="text" name="money"
				value="" placeholder="Только цифры" pattern="[0-9]{1,30}"  required /> <br />
			Role:<br /> <select name="role" required>
				<option value="RENTER">RENTER</option>
				<option value="ADMIN">ADMIN</option>
			</select> <br /> IsBloced<br /> <label> <input type="radio" value="0"
				name="select" checked>Isn't blocked<br>
			</label> <label> <input type="radio" value="1" name="select">Blocked<br>
			</label> ${errorLoginPassMessage} <br /> ${wrongAction} <br /> ${nullPage} <br />
			<input type="submit" value="Create user" />
		</form>
	</div>
</body>
</html>