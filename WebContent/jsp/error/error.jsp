<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
    value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
    scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="resources.pagecontent" />
<html>
<head>
    <title>Error Page</title>
</head>
<body>
Request from ${pageContext.errorData.requestURI} is failed
<br/> Servlet name or type: ${pageContext.errorData.servletName}
<br/> Status code: ${pageContext.errorData.statusCode}
<br/> Exception: ${pageContext.errorData.throwable}
<br/> ${errorQueryPassMessage}
<%-- <c:url value="/index.html" var="mainUrl"/> --%>
<br/> <a href="/BicyclesRent/">Return to main page</a>

</body>

</html>