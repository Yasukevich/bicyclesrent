<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="resources.pagecontent" />
<head>
<title>Login</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Oswald:400,300"
	type="text/css">
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>

</head>
<body>

	<div id="wrapper">
	
		<c:import url="jsp\common\header.jsp" />

		<div id="heading"></div>
		<aside>
			<nav>
				<ul class="aside-menu">
					<li class="active"><a href="controller?command=all_bicycles"><fmt:message
								key="label.all_bicycles" /></a></li>
					<li><a href="controller?command=cargo_bicycles"><fmt:message
								key="label.cargo_bicycles" /></a></li>
					<li><a href="controller?command=mountain_bicycles"><fmt:message
								key="label.mountain_bicycles" /></a></li>
					<li><a href="controller?command=sport_bicycles"><fmt:message
								key="label.sport_bicycles" /></a></li>
					<li><a href="controller?command=urban_bicycles"><fmt:message
								key="label.urban_bicycles" /></a></li>
				</ul>
			</nav>
			<h2>
				<fmt:message key="label.offices" />
			</h2>
			<p>
				<img src="${ pageContext.request.contextPath }/img/offices.jpg"
					width="230" height="180" alt="Our offices">
			</p>
		</aside>
		<section>
			<blockquote>
				<p><fmt:message key="text.h" /></p>
				<cite>Bicycles rent</cite>
			</blockquote>

			<p><fmt:message key="text.p" /></p>


			<figure>
				<img src="${ pageContext.request.contextPath }/img/bicycle.jpg"
					width="320" height="175" alt="">
			</figure>
			<figure>
				<img src="${ pageContext.request.contextPath }/img/bicycle2.jpg"
					width="320" height="175" alt="">
			</figure>
		</section>
	</div>

	<c:import url="\jsp\common\footer.jsp" />


</body>
</html>