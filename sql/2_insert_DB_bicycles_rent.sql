-- country
INSERT INTO `country` (`id`,`name`) VALUES ('DE','Germany');
INSERT INTO `country` (`id`,`name`) VALUES ('EN','England');
INSERT INTO `country` (`id`,`name`) VALUES ('FR','Frence');
INSERT INTO `country` (`id`,`name`) VALUES ('RU','Russia');
INSERT INTO `country` (`id`,`name`) VALUES ('CN','Chaina');

-- street
INSERT INTO `street` (`name`) VALUES ('Victory Square');
INSERT INTO `street` (`name`) VALUES ('Independence Avenue');
INSERT INTO `street` (`name`) VALUES ('Cosmonauts');
INSERT INTO `street` (`name`) VALUES ('Kuprevich');
INSERT INTO `street` (`name`) VALUES ('Zhukov');

-- rent_point
INSERT INTO `rent_point` (`name`, `street_id`) VALUES ('Sport life', 1);
INSERT INTO `rent_point` (`name`, `street_id`) VALUES ('Sport time', 1);
INSERT INTO `rent_point` (`name`, `street_id`) VALUES ('Energy', 2);
INSERT INTO `rent_point` (`name`, `street_id`) VALUES ('Athletics', 3);
INSERT INTO `rent_point` (`name`, `street_id`) VALUES ('Drive', 4);
INSERT INTO `rent_point` (`name`, `street_id`) VALUES ('Healhty life', 5);

-- produser
INSERT INTO `produser` (`name`, `country_id`) VALUES ('BerGaMont', 'DE');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('BMW', 'DE');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('Boardman Bikes', 'EN');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('Brave Machine', 'DE');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('Brompton', 'EN');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('Canyon Bicycles', 'DE');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('Corima', 'FR');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('Corratec', 'DE');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('CUBE Bikes', 'DE');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('Cyfac International', 'FR');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('Dawes Cycles', 'EN');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('Derby International', 'DE');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('Diamant Fahrradwerke AG', 'DE');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('Format', 'RU');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('Falcon Cycles Ltd.', 'EN');
INSERT INTO `produser` (`name`, `country_id`) VALUES ('Flying Pigeon Cycle', 'CN');

-- user
INSERT INTO `user` (`login`, `password`, `role`, `passport`, `money`, `is_blocked`)
VALUES ('Ann', 111, 'renter', 'PM1111111', 1000, 1);
INSERT INTO `user` (`login`, `password`, `role`, `passport`, `money`, `is_blocked`)
VALUES ('Olga', 222, 'renter', 'PM2222222', 300, 1);
INSERT INTO `user` (`login`, `password`, `role`, `passport`, `money`, `is_blocked`)
VALUES ('Den', 333, 'renter', 'PM3333333', 550, 1);
INSERT INTO `user` (`login`, `password`, `role`, `passport`, `money`, `is_blocked`)
VALUES ('Bob', 444, 'renter', 'PM4444444', 700, 1);
INSERT INTO `user` (`login`, `password`, `role`, `passport`, `money`, `is_blocked`)
VALUES ('Rob', 555, 'renter', 'PM5555555', 200, 1);
INSERT INTO `user` (`login`, `password`, `role`, `passport`, `money`, `is_blocked`)
VALUES ('Nina', 666, 'renter', 'PM6666666', 0, 0);
INSERT INTO `user` (`login`, `password`, `role`, `passport`, `money`, `is_blocked`)
VALUES ('Zina', 777, 'renter', 'PM7777777', 600, 1);
INSERT INTO `user` (`login`, `password`, `role`, `passport`, `money`, `is_blocked`)
VALUES ('Tom', 888, 'renter', 'PM8888888', 550, 1);
INSERT INTO `user` (`login`, `password`, `role`, `passport`, `money`, `is_blocked`)
VALUES ('Igar', 999, 'renter', 'PM9999999', 999, 1);
INSERT INTO `user` (`login`, `password`, `role`, `passport`, `money`, `is_blocked`)
VALUES ('Kat', 101010, 'renter', 'PM1010101', 720, 1);
INSERT INTO `user` (`login`, `password`, `role`)
VALUES ('root', 000, 'admin');

-- bicycle
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('��1295', 'Patriot', 'urban', 14, 2018, 80, 1);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('��1296', 'Patriot', 'urban', 14, 2018, 80, 3);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('��1297', 'Patriot', 'urban', 14, 2018, 80, 4);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('��1298', 'Patriot', 'urban', 14, 2018, 80, 5);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('��1299', 'Patriot', 'urban', 14, 2018, 80, 6);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('���12020', 'Lady', 'urban', 14, 1987, 10, 1);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('���12021', 'Lady', 'urban', 14, 1987, 10, 3);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('���12022', 'Lady', 'urban', 14, 1987, 10, 4);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('���12023', 'Lady', 'urban', 14, 1987, 10, 5);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('A2011', 'Forward', 'cargo', 6, 2015, 50, 3);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('A2012', 'Forward', 'cargo', 6, 2015, 50, 4);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('A2013', 'Forward', 'cargo', 6, 2015, 50, 5);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('A2014', 'Forward', 'cargo', 6, 2015, 50, 6);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('���12015', 'Basic ', 'sport', 16, 2010, 30, 3);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('���12016', 'Basic ', 'sport', 16, 2010, 30, 5);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('�32084', 'Orion ', 'sport', 7, 2018, 70, 1);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('�32085', 'Orion ', 'sport', 7, 2018, 70, 3);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('�32086', 'Orion ', 'sport', 7, 2018, 70, 4);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('�32087', 'Orion ', 'sport', 7, 2018, 70, 5);
INSERT INTO `bicycle` (`inventory_num`, `model`, `type`, `produser_id`, `release_year`, `rent_price`, `rent_point_id`)
VALUES ('�32088', 'Orion ', 'sport', 7, 2018, 70, 6);

-- order
INSERT INTO `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days', 'real_return_date`, `revenue`)
VALUES ('�32084', 1, '2018-08-01', 7, '2018-08-07', 350);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('A2013', 8, '2018-08-01', 5, '2018-08-07', 250);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('��1299', 10, '2018-08-04', 10, '2018-08-14', 800);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('A2011', 10, '2018-08-14', 3, '2018-08-17', 150);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('���12023', 12, '2018-08-03', 6, '2018-08-09', 60);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('���12023', 5, '2018-08-09', 4, '2018-08-13', 40);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('���12020', 4, '2018-08-10', 4, '2018-08-14', 40);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('�32084', 4, '2018-08-15', 5, '2018-08-20', 350);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('�32087', 7, '2018-08-09', 2, '2018-08-11', 140);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('A2012', 11, '2018-08-03', 4, '2018-08-07', 200);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('A2012', 1, '2018-08-07', 5, '2018-08-12', 250);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('���12023', 9, '2018-08-23', 2, '2018-08-25', 100);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('A2011', 10, '2018-08-23', 2, '2018-08-25', 100);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('��1298', 11, '2018-08-15', 5, '2018-08-20', 400);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('���12020', 4, '2018-08-15', 3, '2018-08-18', 30);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('���12015', 1, '2018-08-15', 3, '2018-08-18', 90);
insert into `order` (`bicycle_inventory_num`, `user_id`, `start_date`, `amount_of_days`, `real_return_date`, `revenue`)
values ('��1297', 6, '2018-08-19', 4, '2018-08-23', 320);