-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema bicycles_rent
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bicycles_rent
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bicycles_rent` DEFAULT CHARACTER SET utf8 ;
USE `bicycles_rent` ;

-- -----------------------------------------------------
-- Table `bicycles_rent`.`image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycles_rent`.`image` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `image` BLOB NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Table stores images of bicycles';


-- -----------------------------------------------------
-- Table `bicycles_rent`.`country`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycles_rent`.`country` (
  `id` CHAR(2) NOT NULL COMMENT 'Country\'s id',
  `name` VARCHAR(45) NOT NULL COMMENT 'Country\'s name',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  FULLTEXT INDEX `fulltext_country` (`name` ASC)  COMMENT 'find by country')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Country\'s table - dictionary';


-- -----------------------------------------------------
-- Table `bicycles_rent`.`producer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycles_rent`.`producer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Produser\'s id',
  `name` VARCHAR(45) NOT NULL COMMENT 'Produser\'s name',
  `country_id` CHAR(2) NOT NULL COMMENT 'Country\'s id where the producer is located',
  PRIMARY KEY (`id`),
  INDEX `fk_country_id_idx` (`country_id` ASC),
  CONSTRAINT `fk_country_id`
    FOREIGN KEY (`country_id`)
    REFERENCES `bicycles_rent`.`country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 17
DEFAULT CHARACTER SET = utf8
COMMENT = 'Table shows info about produser';


-- -----------------------------------------------------
-- Table `bicycles_rent`.`rent_point`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycles_rent`.`rent_point` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Point\'s id',
  `name` VARCHAR(45) NOT NULL COMMENT 'Point\'s name',
  `street_id` INT(11) NOT NULL COMMENT 'rent point\'s location',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  INDEX `fk_street_id_idx` (`street_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COMMENT = 'Table shows info about point of rent';


-- -----------------------------------------------------
-- Table `bicycles_rent`.`bicycle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycles_rent`.`bicycle` (
  `inventory_num` VARCHAR(15) NOT NULL COMMENT 'inventory number of bicycle',
  `model` VARCHAR(45) NULL DEFAULT NULL COMMENT 'bicycle\'s model',
  `type` ENUM('SPORT', 'CARGO', 'URBAN', 'MOUNTAIN') NOT NULL COMMENT 'bicycle\'s type',
  `producer_id` INT(11) NOT NULL COMMENT 'bicycle\'s produser id',
  `description` VARCHAR(45) NOT NULL COMMENT 'description of bicycle',
  `release_year` YEAR(4) NULL DEFAULT NULL COMMENT 'bicycle\'s release year',
  `rent_price` DECIMAL(10,0) NULL DEFAULT NULL COMMENT 'bicycle\'s price a day',
  `rent_point_id` INT(11) NOT NULL COMMENT 'the point on which the bicycle is located',
  `image_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`inventory_num`),
  INDEX `fk_produser_id_idx` (`producer_id` ASC),
  INDEX `fk_rent_point_id_idx` (`rent_point_id` ASC),
  INDEX `index_price` (`rent_price` ASC)  COMMENT 'find by price',
  INDEX `fk_image_id_idx` (`image_id` ASC),
  CONSTRAINT `fk_image_id`
    FOREIGN KEY (`image_id`)
    REFERENCES `bicycles_rent`.`image` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_produser_id`
    FOREIGN KEY (`producer_id`)
    REFERENCES `bicycles_rent`.`producer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_rent_point_id`
    FOREIGN KEY (`rent_point_id`)
    REFERENCES `bicycles_rent`.`rent_point` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Table shows info about bicycle';


-- -----------------------------------------------------
-- Table `bicycles_rent`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycles_rent`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'User\'s id',
  `login` VARCHAR(45) NOT NULL COMMENT 'User\'s login',
  `password` BIGINT(255) NOT NULL COMMENT 'User\'s password',
  `role` ENUM('ADMIN', 'RENTER') NOT NULL COMMENT 'User\'s role',
  `passport` CHAR(10) NULL DEFAULT NULL COMMENT 'user\'s passort number',
  `money` DECIMAL(10,0) NULL DEFAULT NULL COMMENT 'User\'s money (for rol administrator will be null)',
  `is_blocked` TINYINT(1) NULL DEFAULT NULL COMMENT 'Bloced user (for rol administrator will be null)',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC),
  UNIQUE INDEX `passport_UNIQUE` (`passport` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8
COMMENT = 'Table shows info about user';


-- -----------------------------------------------------
-- Table `bicycles_rent`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycles_rent`.`order` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bicycle_inventory_num` VARCHAR(15) NOT NULL COMMENT 'Bicicle\'s inventory number',
  `user_id` INT(11) NOT NULL COMMENT 'User\'s id',
  `start_date` DATE NOT NULL COMMENT 'Date when user took item in a rent',
  `amount_of_days` INT(11) NOT NULL COMMENT 'Rental period in a days',
  `real_return_date` DATE NOT NULL COMMENT 'Date when user returned item',
  `revenue` DECIMAL(10,0) NOT NULL COMMENT 'order\'s revenue',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `bycicle_start_unique` (`bicycle_inventory_num` ASC, `start_date` ASC),
  UNIQUE INDEX `bycicle_end_unique` (`bicycle_inventory_num` ASC, `real_return_date` ASC),
  INDEX `fk_bicycle_inventory_num_idx` (`bicycle_inventory_num` ASC),
  INDEX `fk_user_id_idx` (`user_id` ASC),
  CONSTRAINT `fk_bicycle_inventory_num`
    FOREIGN KEY (`bicycle_inventory_num`)
    REFERENCES `bicycles_rent`.`bicycle` (`inventory_num`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `bicycles_rent`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = utf8
COMMENT = 'Table shows which bicycle in demand';


-- -----------------------------------------------------
-- Table `bicycles_rent`.`street`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycles_rent`.`street` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'street\'s id',
  `name` VARCHAR(45) NOT NULL COMMENT 'street\'s name',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  FULLTEXT INDEX `fulltext_street` (`name` ASC)  COMMENT 'find by street')
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COMMENT = 'Street\'s table - dictionary';

USE `bicycles_rent`;

DELIMITER $$
USE `bicycles_rent`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `bicycles_rent`.`bicycle_AFTER_UPDATE`
AFTER UPDATE ON `bicycles_rent`.`bicycle`
FOR EACH ROW
BEGIN

END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;