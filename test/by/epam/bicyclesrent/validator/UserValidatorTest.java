package by.epam.bicyclesrent.validator;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class UserValidatorTest {

    /**
     * DataProvider for method validateLogin.
     *
     * @return array for test
     */
    @DataProvider(name = "logins")
    public static Object[][] login() {
        return new Object[][] { { "Ann" }, { "oLgA33" } };
    }

    /**
     * DataProvider for method validateLogin.
     *
     * @return array for test
     */
    @DataProvider(name = "negativeLogins")
    public static Object[][] str() {
        return new Object[][] { { "1" }, { "" }, { "44 Vasia 666.33 true" } };
    }

    /**
     * Validate login test.
     * 
     * @param str
     *            is string for validate
     */
    @Test(dataProvider = "logins")
    public void validateLogin(String str) {
        Assert.assertTrue(UserValidator.validateLogin(str));
    }
    
    /**
     * Negative validate login test.
     * 
     * @param str
     *            is string for validate
     */
    @Test(dataProvider = "negativeLogins")
    public void negativeValidateLogin(String str) {
        Assert.assertFalse(UserValidator.validateLogin(str));
    }
}
