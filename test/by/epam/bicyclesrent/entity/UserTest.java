package by.epam.bicyclesrent.entity;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * test class of User creating.
 */
public class UserTest {
    /**
     * obj User for test.
     */
    User user;

    /**
     * test to check constructor.
     */
    @Test
    public void tst_constructor() {
        user = new User("login", "password", Role.RENTER, "passport", 100, false);
        Assert.assertEquals("login", user.getLogin());
        Assert.assertEquals("password", user.getPassword());
        Assert.assertEquals(Role.RENTER, user.getRole());
        Assert.assertEquals("passport", user.getPassport());
        Assert.assertEquals(100, user.getMoney());
        Assert.assertFalse(user.getIsBlocked());
    }
}
